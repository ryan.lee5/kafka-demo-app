from faker import Faker
from datetime import datetime

fake = Faker()

def get_registered_user():
    now = datetime.now()
    date_time = now.strftime("%m-%d-%Y %H:%M:%S")
    return {
        "start_time": date_time,
        "name": fake.name(),
        "address": fake.address(),
        "created_at": fake.year()
    }
    
if __name__ == "__main__":
    print(get_registered_user())
