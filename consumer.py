from kafka import KafkaConsumer
import json

BOOTSTRAP_SERVER = ["b-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094","b-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094","b-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094"]

if __name__ == "__main__":
    consumer = KafkaConsumer(
        "interaction_data_topic",
        bootstrap_servers=BOOTSTRAP_SERVER,
        auto_offset_reset="earliest",
        group_id="consumer-group-a")

    print("starting the consumer")

    for msg in consumer:
        print(f'Registered User: {json.loads(msg.value)}')


