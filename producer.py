import json
from kafka import KafkaProducer
from data import get_registered_user
import time


BOOTSTRAP_SERVER = ["b-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094","b-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094","b-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094"]

def json_serializer(data):
    return json.dumps(data).encode('utf-8')

def get_partition(key, all, available):
    # Return partition 0
    return 0



producer = KafkaProducer(bootstrap_servers=BOOTSTRAP_SERVER, value_serializer=json_serializer)


if __name__ == "__main__":
    while True:
        registered_user = get_registered_user()
        print(f"registered_user: {registered_user}")
        producer.send(topic="interaction-data-topic", value=registered_user)
        time.sleep(2)