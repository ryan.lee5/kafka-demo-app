# Kafka Demo App

kafka demo app and configuration.


# ZookeeperConnectString
```
aws kafka describe-cluster --region us-east-1 --cluster-arn arn:aws:kafka:us-east-1:291943606412:cluster/jarvis-dev-cluster/01b85d9e-46f8-4298-82a2-0afc91870805-19

"z-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181"
```


# List topic
```
bin/kafka-topics.sh --list --zookeeper "z-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181"
```


# Create topic
```
bin/kafka-topics.sh --create --zookeeper "z-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181,z-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:2181" --replication-factor 3 --partitions 1 --topic interaction_data_topic
```


# Trust store
Since we use BootstrapBroker using TLS, we need setup truststore as below.  

```
cp /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.302.b08-0.amzn2.0.1.x86_64/jre/lib/security/cacerts /tmp/kafka.client.truststore.jks
```


# Find BootstrapBroker
```
aws kafka get-bootstrap-brokers --region us-east-1 --cluster-arn arn:aws:kafka:us-east-1:291943606412:cluster/jarvis-dev-cluster/01b85d9e-46f8-4298-82a2-0afc91870805-19

"BootstrapBrokerStringTls": 
"b-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094"
```


# Producer example
```
./kafka-console-producer.sh --broker-list "b-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094" --producer.config client.properties --topic interaction_data_topic
```


# Consumer example
```
./kafka-console-consumer.sh --bootstrap-server "b-1.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-3.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094,b-2.jarvis-dev-cluster.rbzi6w.c19.kafka.us-east-1.amazonaws.com:9094" --consumer.config client.properties --topic interaction_data_topic --from-beginning
```